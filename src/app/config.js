'use strict';

import pkg    from '../../package';
import states from 'app/states';

let apiUrl = `http://localhost:10101/`;

let props = {
  pkg      : pkg,
  title    : `ng1-md`,
  app      : {
    title     : `ng1-md`,
    separator : ` | `,
    uri       : window.location.href,
    name      : `ng1-md`,
  },
  logLevel : `info`,
  palettes : [
    {
      name    : `default`,
      primary : `indigo`,
      accent  : `yellow`,
      warn    : `red`,
      range   : null,
    },
  ],
};

export { apiUrl, props, states };
