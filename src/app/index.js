/*
 global angular
 global _
 */

import pkg       from '../../package';
import { props } from './config';

// Require assets that are not added during the webpack build here
require(`../assets/style.less`);
require(`../../node_modules/angular-material/angular-material.min.css`);
require(`../../node_modules/angular-material/angular-material.min`);
require(`../vendor/fontawesome-5.0.6-all`);


const ngModule = angular.module(
  pkg.name,
  [
    `ui.router`,
    `ngMaterial`,
  ])
  .config(($locationProvider, $mdThemingProvider, $compileProvider) => {
    $locationProvider.html5Mode(true);

    props.palettes.forEach((p) => {
      let theme = $mdThemingProvider.theme(p.name)
        .primaryPalette(p.primary)
        .accentPalette(p.accent)
        .warnPalette(p.warn);
      if (p.range === `dark`) {
        theme.dark();
      }
    });

    // Turn off debug logging if not in localhost
    $compileProvider.debugInfoEnabled(window.location.href.indexOf(`localhost`) > -1);
  })
  .run(($log) => {
    $log.info(`Starting: ${pkg.name} v${pkg.version}`);
  });

export default ngModule;

require(`./components`);
require(`./factories`);
require(`./models`);
require(`./services`);
