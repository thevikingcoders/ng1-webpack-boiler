import Ajv from 'ajv';

const ajv = new Ajv({useDefaults : true, $data: true});
require(`ajv-keywords`)(ajv);

export { ajv };
