/* global _ */

import ngModule          from 'app';
import pkg               from '../../../../package';
import { props, states } from "../../config";

require(`./style.less`);

ngModule
  .config(($stateProvider) => {
    $stateProvider
      .state(`app`, {

        abstract : true,
      });
  })
  .component(`appRoot`, {
    template   : require(`./template.html`),
    controller : class {

      constructor($log, $transitions, $mdSidenav, $mdMedia, PageFactory, UserService) {
        this.flex        = {
          gtMd    : 90,
          gtMdOff : 5,
          gtLg    : 70,
          gtLgOff : 15,
        };
        this.showToolbar = false;

        this.$log         = $log;
        this.$transitions = $transitions;
        this.$mdSidenav   = $mdSidenav;
        this.$mdMedia     = $mdMedia;
        this.pageTitle    = undefined;
        this.pageFactory  = PageFactory;
        this.userSvc      = UserService;
      }

      $onInit() {
        this.$transitions.onStart({}, (trans) => {
          let tFrom = trans.from();
          let tTo   = trans.to();

          this.log(tFrom, `$transitions:from`, `debug`);
          this.log(tTo, `$transitions:to`, `debug`);
        });

        this.$transitions.onSuccess({}, (trans) => {
          let tFrom = trans.from();
          let tTo   = trans.to();

          this.pageTitle = _.find(states, {name : tTo.name}).label;
          this.pageFactory.title(this.pageTitle);

          this.log(tFrom, `$transitions:from`, `debug`);
          this.log(tTo, `$transitions:to`, `debug`);
        });

        this.log(this, `appRoot`);
      }

      log(msg, label, level = props.logLevel) {
        this.$log[level]([pkg.name, label].join(`:`), msg);
      }

      toggle(id) {
        this.$mdSidenav(id).toggle();
      }

      biggerThan(size = `gt-md`) {
        let isBigger = this.$mdMedia(size);
        if (isBigger) {
          this.flex.gtLg    = 90;
          this.flex.gtLgOff = 5;
        } else {
          this.flex.gtLg    = 80;
          this.flex.gtLgOff = 10;
        }
        return isBigger;
      }

      showLeft() {
        return this.biggerThan();
      }

    }
  });
