/* global _ */

import ngModule          from 'app';
import { props, states } from 'app/config';

require(`./style.less`);

ngModule
  .component(`appSidebarLeft`, {
    bindings   : {},
    require    : {
      root : `^^appRoot`,
    },
    template   : require(`./template.html`),
    controller : class {

      constructor($log) {
        this.$log     = $log;
        this.appTitle = props.app.title;
        this.states   = states;
      }

      $onInit() {
        this.showToolbar = this.root.showToolbar;
        this.log(this);
      }

      log(msg, label, level) {
        this.root.log(msg, [`appSidebarLeft`, label].join(`:`), level);
      }

    }
  });
