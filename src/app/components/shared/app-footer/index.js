/* global _ */

import ngModule from 'app';

require(`./style.less`);

ngModule
  .component(`appFooter`, {
    bindings   : {},
    require    : {
      root : `^^appRoot`,
    },
    template   : require(`./template.html`),
    controller : class {

      constructor($log) {
        this.$log  = $log;
        this.title = `New Component`;
      }

      $onInit() {
        this.log(this);
      }

      log(msg, label, level) {
        this.root.log(msg, [`appFooter`, label].join(`:`), level);
      }

    }
  });
