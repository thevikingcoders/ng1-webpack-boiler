/* global _ */

import ngModule   from 'app/index';
import { states } from 'app/config';

require(`./style.less`);

ngModule
  .component(`navBar`, {
    bindings   : {},
    require    : {
      root : `^^appRoot`,
    },
    template   : require(`./template.html`),
    controller : class {

      constructor($log, $transitions) {
        this.$log         = $log;
        this.$transitions = $transitions;
        this.currentNav   = undefined;
        this.states       = states;
      }

      $onInit() {
        this.$transitions.onSuccess({}, (trans) => {
          let tFrom = trans.from();
          let tTo   = trans.to();
          this.log(tFrom, `$transitions:from`);
          this.log(tTo, `$transitions:to`);
          this.currentNav = tTo.name;
        });

        this.log(this);
      }

      log(msg, label, level) {
        this.root.log(msg, [`navBar`, label].join(`:`), level);
      }

    }
  });
