/* global _ */

import ngModule  from 'app';
import { props } from 'app/config';

require(`./style.less`);

ngModule
  .component(`appHeader`, {
    bindings   : {},
    require    : {
      root : `^^appRoot`,
    },
    template   : require(`./template.html`),
    controller : class {

      constructor($log) {
        this.$log         = $log;
        this.appTitle     = props.app.title;
      }

      $onInit() {
        this.showToolbar = this.root.showToolbar;
        this.log(this);
      }

      log(msg, label, level = `info`) {
        this.root.log(msg, [`appHeader`, label].join(`:`), level);
      }

    }
  });
