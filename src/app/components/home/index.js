/* global _ */

import ngModule from 'app';

require(`./style.less`);

ngModule
  .config(($stateProvider) => {
    $stateProvider
      .state(`app.home`, {
        url       : `/`,
        component : `appHome`,
      });
  })
  .component(`appHome`, {
    bindings   : {},
    require    : {
      root : `^^appRoot`,
    },
    template   : require(`./template.html`),
    controller : class {

      constructor($log) {
        this.$log  = $log;
        this.title = `New Component`;
      }

      $onInit() {
        this.log(this);
      }

      log(msg, label, level) {
        this.root.log(msg, [`appHome`, label].join(`:`), level);
      }

    }
  });
