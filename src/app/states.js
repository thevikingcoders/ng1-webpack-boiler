export default [
  {
    name  : `app.home`,
    label : `Home`,
    icon  : `fa-home`,
  },
  {
    name  : `app.sample`,
    label : `Sample 1`,
    icon  : `fa-link`,
    states : [
      {
        name  : `app.sample.page1`,
        label : `Page 1`
      }
    ]
  },
  {
    name  : `app.sample2`,
    label : `Sample 2`,
    icon  : `fa-link`,
  }
];
