/* global _ */

import ngModule  from 'app';
import { props } from 'app/config';

ngModule.service(`UserService`, class {

  constructor($log) {
    this.$log = $log;
    this.log(this);
  }

  log(msg, label, level = props.logLevel) {
    this.$log[level]([props.pkg.name, `UserService`, label].join(`:`), msg);
  }

});
