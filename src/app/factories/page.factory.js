/* global _*/

import ngModule  from 'app';
import { props } from '../config';

ngModule.factory(`PageFactory`, ($log, $window) => {

  let ps = {};

  ps.title = (title) => {
    title = title ? _.castArray(title) : [];
    title.push(props.app.title);
    $window.document.title = title.join(props.app.separator);
  };

  return ps;

});
