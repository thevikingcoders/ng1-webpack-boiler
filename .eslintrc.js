module.exports = {
  "env": {
    "es6": true,
    "node": true,
    "mocha": true,
    "browser": true,
  },
  "extends": `eslint:recommended`,
  "parserOptions": {
    "sourceType": `module`,
  },
  "rules": {
    "indent": [
      `error`,
      2,
    ],
    "quotes": [
      `error`,
      `backtick`,
    ],
    "no-console": [
      `error`,
      {
        "allow": [`log`, `info`, `warn`, `error`],
      },
    ],
    "yoda": [
      `error`
    ],
    "no-unused-vars": [
      `warn`,
      {
        "vars": `local`,
      }
    ],
  },
};
