/* global _ */

import ngModule  from 'app';
import { props } from 'app/config';

ngModule.service(`ServiceName`, class {

  constructor($log) {
    this.$log = $log;
    this.log(this);
  }

  log(msg, label, level = props.logLevel) {
    this.$log[level](`${[props.pkg.name, `ServiceName`, label].join(`:`)}`, msg);
  }

});
