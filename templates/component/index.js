/* global _ */

import ngModule from 'app';

const component_name = `appSupportFaqs`;
const state_name = ``;

require(`./style.less`);

ngModule
  .config(($stateProvider) => {
    $stateProvider
      .state(state_name, {
        url       : ``,
        component : component_name,
      });
  })
  .component(component_name, {
    bindings   : {},
    require    : {
      root : `^^appRoot`,
    },
    template   : require(`./template.html`),
    controller : class {

      constructor($log) {
        this.$log  = $log;
        this.title = `New Component`;
      }

      $onInit() {
        this.log(this);
      }

      log(msg, label, level) {
        this.root.log(msg, [component_name, label].join(`:`), level);
      }

    }
  });
