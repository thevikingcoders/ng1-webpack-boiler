const path              = require(`path`);
const webpack           = require(`webpack`);
const env               = (process.env.NODE_ENV || `development`).toLowerCase();
const port              = process.env.PORT || 8888;
const _                 = require(`lodash`);
const pkg               = require(`./package.json`);
const HtmlWebpackPlugin = require(`html-webpack-plugin`);

const baseHref = env === `production` ? `` : `http://localhost:${port}/`;

let envConfig  = require(`./webpack/${env}`);
let entries    = {
  app    : path.resolve(__dirname, `./src/app/index.js`),
  ng     : [
    `angular`,
    `angular-animate`,
    `angular-aria`,
    `angular-messages`,
    `@uirouter/angularjs`,
  ],
  vendor : [
    `lodash`,
    `moment`,
  ],
};

let outputPath = path.resolve(__dirname, `static`);

let config = {
  entry     : entries,
  output    : {
    filename : envConfig.output ? envConfig.output.filename : `[name].[chunkhash].js`,
    path     : outputPath,
  },
  context   : outputPath,
  module    : envConfig.module,
  node      : {
    fs : `empty`,
  },
  externals : {
    jQuery : `jquery`,
    $      : `jquery`,
    _      : `lodash`,
  },
  resolve   : {
    modules : [
      path.resolve(`src`),
      `node_modules`,
    ],
  },
};

let rules = [
  {
    test    : [/\.js$/, /\.es6$/],
    exclude : /(node_modules|bower_components)/,
    use     : [`ng-annotate-loader`, `babel-loader`],
  },
  {
    test : [/\.css$/],
    use  : [`style-loader`, `css-loader`],
  },
  {
    test    : /\.html$/,
    exclude : /\.tpl\.html/,
    loader  : `html-loader`,
  },
  {
    test   : /\.svg$/,
    loader : `svg-inline-loader`,
  },
  {
    test   : /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
    loader : `url-loader?limit=10000&mimetype=application/font-woff`,
  },
  {
    test   : /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
    loader : `url-loader?limit=10000&mimetype=application/octet-stream`,
  },
  {
    test   : /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    loader : `file-loader`,
  },
  {
    test   : /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    loader : `url-loader?limit=10000&mimetype=image/svg+xml`,
  },
];

config.module.rules = config.module.rules.concat(rules);

config.plugins = [
  new webpack.optimize.CommonsChunkPlugin({
                                            name      : Object.keys(entries),
                                            minChunks : Infinity,
                                          }),
  new webpack.optimize.CommonsChunkPlugin({
                                            name      : `manifest`,
                                            minChunks : Infinity,
                                          }),
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  new HtmlWebpackPlugin({
                          title    : `Loading...`,
                          aName    : pkg.name,
                          filename : `index.html`,
                          template : `../src/templates/index.tpl.html`,
                          baseHref : baseHref,
                        }),
  new webpack.ProvidePlugin({
                              $      : `jquery`,
                              jQuery : `jquery`,
                            }),
];

let moduleExport = _.merge(config, envConfig);

module.exports = moduleExport;
