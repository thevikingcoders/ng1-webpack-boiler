# ng1-webpack-boiler

### Requirements

* Node version >= 8
* NPM version >= 5
  * _Yarn is recommended for ease of use:_ [Yarn](https://yarnpkg.com/en/docs/install)
* NVM
  * Mac OS X : https://github.com/creationix/nvm
  * Windows  : https://github.com/coreybutler/nvm-windows

#### Windows
[Git Bash](https://git-scm.com/downloads)


## Getting Started

#### Installation
```bash
$ git clone https://bitbucket.org/thevikingcoders/ng1-webpack-boiler.git
$ cd ng1-webpack-boiler
# Install required packages
$ yarn
# or 
$ npm install
```

#### Startup
```bash
$ yarn serve
# or
$ npm run serve
```

Open browser to [localhost:8888](http://localhost:8888)

#### Build Process

This will create the needed files in the /static/ directory. (_Work in Progress_) 

```bash
$ yarn build
# or
$ npm run build
```

## Node Modules
(_TBD_)

## Optional Node Modules (Global)

| Package | URI |
| :------ | --- |
| eslint  | https://www.npmjs.com/package/eslint |
| unit.js | http://unitjs.com/guide/quickstart.html |
| mocha   | https://www.npmjs.com/package/mocha |
| sinon   | https://www.npmjs.com/package/sinon |

```bash
$ npm install -g eslint unit.js mocha sinon
```


