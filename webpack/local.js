/**
 * webpack.local.js
 */
const _ = require(`lodash`);

const devConfig = require(`./development`);

const config = {};

config.module = {
  rules: devConfig.module.rules
};

module.exports = _.merge(devConfig, config);
