/**
 * webpack.development.js
 */
const path = require(`path`);

let devServerPort = process.env.PORT || 8888;

const config = {};

config.module = {
  rules: [
    {
      test: /\.less$/,
      use: [{
        loader: `style-loader` // creates style nodes from JS strings
      }, {
        loader: `css-loader` // translates CSS into CommonJS
      }, {
        loader: `less-loader` // compiles Less to CSS
      }]
    }
  ]
};

const outputPath = path.resolve(__dirname, `../dist`);
console.log(`outputPath: `, outputPath);

config.devServer = {
  contentBase: outputPath,
  compress: true,
  port: devServerPort,
  historyApiFallback: true,
};

module.exports = config;
