/**
 * webpack.production.js
 */
const webpack = require(`webpack`);
const ExtractTextPlugin = require(`extract-text-webpack-plugin`);

const extractLess = new ExtractTextPlugin({
  filename: `[name].[contenthash].css`
});

const config = {
  output: {
    filename: `[name].[chunkhash].min.js`
  }
};

config.module = {
  rules: [
    {
      test: /\.less$/,
      use: extractLess.extract({
        use: [{
          loader: `css-loader`
        }, {
          loader: `less-loader`
        }]
      })
    },
  ]
};

config.plugins = [
  extractLess,
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
      drop_console: false,
    }
  }),
];

module.exports = config;
